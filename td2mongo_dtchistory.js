/*==========================================================
 * node td2mongo_dtchistory.js  [no parameters]
 * 2016.12.22
 * eunho.kim
*============================================================*/

//include libraries
var jsforce = require('jsforce');
var TD = require('td');
var mongoose = require('mongoose');
var async = require('async');
var csv = require('csv');
var fs = require('fs');
var sl = require('sleep');
var Set = require("collections/set");
var Map = require("collections/map");
var util = require('util');
var moment = require('moment');
var empty = require('is-empty');
var valueDecimal = require('./getNumberOfDigits.js');




//generate new treasure data client
console.log('==============TD_API_KEY = ' + process.env.TD_API_KEY);
var client = new TD(process.env.TD_API_KEY);



//open a new salesforce connection
var conn = new jsforce.Connection({loginUrl : process.env.SFDC_URL});


//open a new mongoDB connection
//http://blog.mlab.com/2014/04/mongodb-driver-mongoose/
var Schema   = mongoose.Schema;
var GraphSchema = new Schema({ graphKey: String, name:String, data: Object, unit:String, valueDecimals:Object});
mongoose.model('Graph', GraphSchema);
//mongoose.connect(process.env.MONGODB_URI);
mongoose.connect(process.env.MONGODB_URI + '?connectTimeoutMS=300000');
console.log(process.env.MONGODB_URI);







//input parameteres--------------------------------------
var theDay = process.argv[2];
console.log('=============parameter = ' + theDay);
//-------------------------------------------------------




//reading sql files--------------------------------
async.waterfall([


  

  //read sql file (td_dtchistory)================================== 
  function(callback){
    var SQL_TD_DTCHISTORY = fs.readFile('/home/ubuntu/TDSQL_DTCHISTORY3.sql', 'utf8', function(err, data){
      
      if(err){console.error(err);process.exit(1);}
      
      SQL_TD_DTCHISTORY = util.format(data, theDay);
      
      callback(null, SQL_TD_DTCHISTORY);
    });
    
  },





  //process td_history=============================
  function(sql, callback){
    //get td_history data-------------------------------
    client.prestoQuery(process.env.TD_DATABASES, sql, function(err, results) {
      
      if(err){console.error(err);process.exit(1);}

      console.log('=============TD job id = ' + results.job_id);
      
      sl.sleep(10);

      var MAX_COUNT = 6;    //for 1 minute
      var i = 0;

      (function loop() {
        
        if(i < MAX_COUNT){
          
          client.showJob(results.job_id, function(err, r){
            
            if(err){console.error(err);process.exit(1);}
            
            console.log('=============job status = ' + r.status);

            if(r && r.status == 'success'){

              console.log('=============job completed : querying td_history');
              
              var j = 0;
              client.jobResult(results.job_id, 'csv', function(err, tdData){
                
                if(err){console.error(err);process.exit(1);}
                
                console.log('=======' + Object.keys(tdData).length);
                if(Object.keys(tdData).length == 0){console.error(err);process.exit(0);}

                csv.parse(tdData, function(err, rawData){

                  if (err) { console.error(err);process.exit(1); }

                  //if(rawData.length == 0){ console.error(err);process.exit(1); }
                  
                  var DTCs = new Set();
                  var startTime;
                  var endTime;
                  
                  rawData.forEach(function (line, idx) {
                    DTCs.add(line);
                    if(!startTime || startTime > line[5]) startTime = line[5];
                    if(!endTime || endTime < line[6]) endTime = line[6];
                  });

                  console.log('=============startTime = ' + startTime);
                  console.log('=============endTime = ' + endTime);

                  callback(null, DTCs, startTime, endTime);

                }); // end of csv parse (td_history)
              }); // end of job result (td_history)
            }else{
              console.log('============this job is still processing...  waiting 10 seconds');
              sl.sleep(10);
              i++;
              loop();  
            }
          }); //end of show job (td history)
        }
      }()); //end of loop (td_history)
    }); // end of presto query
    //------------------------------------------------------------
  },






  //read sql file (j1939 + kwpccp)================================== 
  function(token, startTime, endTime, callback){
    
    fs.readFile('/home/ubuntu/TDSQL_DTCHISTORY2.sql', 'utf8', function(err, data){
      
      if(err){console.error(err);process.exit(1);}

      //apply parameters to sql--------------------------
      var SQL_UNITED = util.format(data, startTime, endTime);
      //console.log('================SQL_UNITED = ' + SQL_UNITED);
      //-------------------------------------------------
      callback(null, SQL_UNITED, token);
    });
  },




  //process united resultset=========================================
  function(sql, token, callback){

    //get united data-------------------------------
    client.prestoQuery(process.env.TD_DATABASES, sql, function(err, results) {
      
      if(err){console.error(err);process.exit(1);}

      console.log('=============TD job id = ' + results.job_id);
      
      sl.sleep(10);

      var MAX_COUNT = 6;    //for 1 minute
      var i = 0;

      (function loop() {
        
        if(i < MAX_COUNT){
          
          client.showJob(results.job_id, function(err, r){
            
            if(err){console.log(err);process.exit(1);}
            
            console.log('=============job status = ' + r.status);

            if(r && r.status == 'success'){

              console.log('=============job completed : querying united table');
              
              var j = 0;
              client.jobResult(results.job_id, 'csv', function(err, tdData){

                if(err){console.error(err);process.exit(1);}

                if(empty(tdData)){console.log('==========no data patched');process.exit(0);}

                csv.parse(tdData, function(err, rawData){

                  console.log('=============csv parse');

                  if (err) { console.error(err);process.exit(1); }


                  //json data templates of mongoDB--------------------------------------------------
                  var dtc_map = new Map();
                  var info_map = new Map();

                  var _ECU = new Set([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33]);
                  var _ACU = new Set([0,1,2,3,24,26,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59]);

                  var indexOfValueDecimal = [53, 54, 55, 56, 57, 58, 59]; // CCP項目
                  var charts = [];                  

                  token.forEach(function(t, tidx){

                    //var charts = [];
                    var foreignKey = t[0];
                    var dtcType = t[2];
                    console.log('===========foreignKey = ' + foreignKey);
                    info_map.set(foreignKey, t);

                    //xAxis--------------------------------
                    charts['0'] = {"graphKey": foreignKey + "-xAxis","data":[], "name":"xAxis"};

                    if('ECU' == dtcType){

                      charts['1'] = {"graphKey": foreignKey + "-engine_revolution", "name": "Engine Revolution","data": [],"unit": " rpm","valueDecimals": 0};
                      charts['2'] = {"graphKey": foreignKey + "-final_fuel_quantity", "name": "Final Fuel Quantity","data": [],"unit": " mm3/st","valueDecimals": 8};
                      charts['3'] = {"graphKey": foreignKey + "-coolant_water_temperature", "name": "Coolant Water Temperature","data": [],"unit": " degC","valueDecimals": 0};
                      charts['4'] = {"graphKey": foreignKey + "-intake_manifold_pressure", "name": "Intake Manifold Pressure","data": [],"unit": " kpa","valueDecimals": 0};
                      charts['5'] = {"graphKey": foreignKey + "-final_injection_timing", "name": "Final Injection Timing","data": [],"unit": " CA","valueDecimals": 1};
                      charts['6'] = {"graphKey": foreignKey + "-pre_injection_fuel_quantity", "name": "Pre-Injection Fuel Quantity","data": [],"unit": " mm3/st","valueDecimals": 8};
                      charts['7'] = {"graphKey": foreignKey + "-pre_injection_interval", "name": "Pre-Injection Interval","data": [],"unit": " usec","valueDecimals": 0};
                      charts['8'] = {"graphKey": foreignKey + "-neutral_switch", "name": "Neutral Switch","data": [],"unit": "","valueDecimals": 0};
                      charts['9'] = {"graphKey": foreignKey + "-dpf_status", "name": "DPF Status","data": [],"unit": "","valueDecimals": 0};
                      charts['10'] = {"graphKey": foreignKey + "-parking_sw", "name": "Parking SW","data": [],"unit": "","valueDecimals": 0};

                      charts['11'] = {"graphKey": foreignKey + "-dpf_auto_regeneration_inhibit_sw", "name": "DPF Auto Regeneration Inhibit SW","data": [],"unit": "","valueDecimals": 0};
                      charts['12'] = {"graphKey": foreignKey + "-dpf_manual_regeneration_force_sw", "name": "DPF Manual Regeneration Force SW","data": [],"unit": "","valueDecimals": 0};
                      charts['13'] = {"graphKey": foreignKey + "-pm_sedimentation_quantity1", "name": "PM Sedimentation Quantity1","data": [],"unit": " mg","valueDecimals": 0};
                      charts['14'] = {"graphKey": foreignKey + "-pm_sedimentation_quantity2", "name": "PM Sedimentation Quantity2","data": [],"unit": " mg","valueDecimals": 0};
                      charts['15'] = {"graphKey": foreignKey + "-intake_air_temp", "name": "Intake Air Temp","data": [],"unit": " degC","valueDecimals": 0};
                      charts['16'] = {"graphKey": foreignKey + "-air_mass_flow", "name": "Air Mass Flow","data": [],"unit": " Kg/h","valueDecimals": 1};
                      charts['17'] = {"graphKey": foreignKey + "-post_injection_quantity", "name": "Post Injection quantity","data": [],"unit": " mm3/st","valueDecimals": 8};
                      charts['18'] = {"graphKey": foreignKey + "-exhaust_gas_temp_0", "name": "Exhaust Gas Temp 0","data": [],"unit": " degC","valueDecimals": 0};
                      charts['19'] = {"graphKey": foreignKey + "-exhaust_gas_temp_1", "name": "Exhaust Gas Temp 1","data": [],"unit": " degC","valueDecimals": 0};
                      charts['20'] = {"graphKey": foreignKey + "-exhaust_gas_temp_2", "name": "Exhaust Gas Temp 2","data": [],"unit": " degC","valueDecimals": 0};

                      charts['21'] = {"graphKey": foreignKey + "-differential_pressure_1", "name": "Differential Pressure 1","data": [],"unit": "kPa","valueDecimals": 2};
                      charts['22'] = {"graphKey": foreignKey + "-rgnstatus", "name": "RGNSTATUS","data": [],"unit": "","valueDecimals": 0};
                      charts['23'] = {"graphKey": foreignKey + "-rgnlevel", "name": "RGNLEVEL","data": [],"unit": "","valueDecimals": 0};
                      charts['24'] = {"graphKey": foreignKey + "-battery_voltage", "name": "Battery Voltage","data": [],"unit": " V","valueDecimals": 3};
                      charts['25'] = {"graphKey": foreignKey + "-atmospheric_pressure", "name": "Atmospheric Pressure","data": [],"unit": " kPa","valueDecimals": 0};
                      charts['26'] = {"graphKey": foreignKey + "-accel_pedal_position", "name": "Accel Pedal Position","data": [],"unit": " %","valueDecimals": 1};
                      charts['27'] = {"graphKey": foreignKey + "-actual_rail_pressure", "name": "Actual Rail Pressure","data": [],"unit": " MPa","valueDecimals": 2};
                      charts['28'] = {"graphKey": foreignKey + "-target_rail_pressure", "name": "Target Rail Pressure","data": [],"unit": " MPa","valueDecimals": 0};
                      charts['29'] = {"graphKey": foreignKey + "-actual_scv_current", "name": "Actual SCV Current","data": [],"unit": " mA","valueDecimals": 0};
                      charts['30'] = {"graphKey": foreignKey + "-target_scv_current", "name": "Target SCV Current","data": [],"unit": " mA","valueDecimals": 0};

                      charts['31'] = {"graphKey": foreignKey + "-fuel_temperature", "name": "Fuel Temperature","data": [],"unit": " degC","valueDecimals": 0};
                      charts['32'] = {"graphKey": foreignKey + "-accel_pedal_pos1", "name": "Accel Pedal Pos 1","data": [],"unit": "","valueDecimals": 1};
                      charts['33'] = {"graphKey": foreignKey + "-eng_percent_load_at_current_speed", "name": "Eng Percent Load At Current Speed","data": [],"unit": "%","valueDecimals": 1};

                    

                    }else{
                      charts['1'] = {"graphKey": foreignKey + "-engine_revolution", "name": "Engine Revolution","data": [],"unit": " rpm","valueDecimals": 0};
                      charts['2'] = {"graphKey": foreignKey + "-final_fuel_quantity", "name": "Final Fuel Quantity","data": [],"unit": " mm3/st","valueDecimals": 0};
                      charts['3'] = {"graphKey": foreignKey + "-coolant_water_temperature", "name": "Coolant Water Temperature","data": [],"unit": " degC","valueDecimals": 0};
                      charts['24'] = {"graphKey": foreignKey + "-battery_voltage", "name": "Battery Voltage","data": [],"unit": " V","valueDecimals": 3};
                      charts['26'] = {"graphKey": foreignKey + "-accel_pedal_position", "name": "Accel Pedal Position","data": [],"unit": "","valueDecimals": 0};
                      charts['32'] = {"graphKey": foreignKey + "-accel_pedal_pos1", "name": "Accel Pedal Pos 1","data": [],"unit": "","valueDecimals": 1};
                      charts['33'] = {"graphKey": foreignKey + "-eng_percent_load_at_current_speed", "name": "Eng Percent Load At Current Speed","data": [],"unit": "%","valueDecimals": 1};
                      //ACU only-----
                      charts['34'] = {"graphKey": foreignKey + "-def_level", "name": "DEF level","data": [],"unit": " %","valueDecimals": 1};
                      charts['35'] = {"graphKey": foreignKey + "-requested_dosing_rate", "name": "Requested dosing rate","data": [],"unit": " g/h","valueDecimals": 5};
                      charts['36'] = {"graphKey": foreignKey + "-pump_duty_cycle", "name": "Pump duty cycle","data": [],"unit": "","valueDecimals": 4};
                      charts['37'] = {"graphKey": foreignKey + "-pump_motor_speed", "name": "Pump motor speed","data": [],"unit": " rpm","valueDecimals": 1};
                      
                      charts['38'] = {"graphKey": foreignKey + "-def_pressure", "name": "DEF pressure","data": [],"unit": " kPa","valueDecimals": 1};
                      charts['39'] = {"graphKey": foreignKey + "-inlet_nox", "name": "Inlet NOx","data": [],"unit": " ppm","valueDecimals": 2};
                      charts['40'] = {"graphKey": foreignKey + "-outlet_nox", "name": "Outlet NOx","data": [],"unit": " ppm","valueDecimals": 2};
                      charts['41'] = {"graphKey": foreignKey + "-scr_inlet_temperature", "name": "SCR Inlet Temperature","data": [],"unit": " degC","valueDecimals": 1};
                      charts['42'] = {"graphKey": foreignKey + "-thaw_system_status", "name": "Thaw system status","data": [],"unit": "","valueDecimals": 0};
                      charts['43'] = {"graphKey": foreignKey + "-injector_duty_cycle", "name": "Injector duty cycle","data": [],"unit": "","valueDecimals": 4};
                      charts['44'] = {"graphKey": foreignKey + "-def_temperature", "name": "DEF temperature","data": [],"unit": " degC","valueDecimals": 0};
                      charts['45'] = {"graphKey": foreignKey + "-torque_limit_thaw", "name": "Torque Limit(Thaw)","data": [],"unit": " %","valueDecimals": 2};
                      charts['46'] = {"graphKey": foreignKey + "-torque_limit_dtc", "name": "Torque Limit(DTC)","data": [],"unit": " %","valueDecimals": 2};
                      charts['47'] = {"graphKey": foreignKey + "-def_concentration", "name": "DEF Concentration","data": [],"unit": " %","valueDecimals": 2};
                      
                      charts['48'] = {"graphKey": foreignKey + "-inlet_nox_sensor_status", "name": "Inlet NOx sensor status","data": [],"unit": "","valueDecimals": 0};
                      charts['49'] = {"graphKey": foreignKey + "-outlet_nox_sensor_status", "name": "Outlet NOx sensor status","data": [],"unit": "","valueDecimals": 0};
                      charts['50'] = {"graphKey": foreignKey + "-tank_coolant_valve_on_off", "name": "Tank Coolant Valve ON/OFF","data": [],"unit": " %","valueDecimals": 0};

                      charts['51'] = {"graphKey": foreignKey + "-cooldown_abuser_counter", "name": "Cooldown Abuser Counter","data": [],"unit": "","valueDecimals": 0};
                      charts['52'] = {"graphKey": foreignKey + "-purge_abuse_counter", "name": "Purge Abuse Counter","data": [],"unit": "","valueDecimals": 0};
                      charts['53'] = {"graphKey": foreignKey + "-ambient_temperature", "name": "Ambient temperature","data": [],"unit": " deg","valueDecimals": 0};
                      charts['54'] = {"graphKey": foreignKey + "-torque_limit_acu", "name": "Torque Limit(ACU)","data": [],"unit": " %","valueDecimals": 0};
                      charts['55'] = {"graphKey": foreignKey + "-torque_limit_application", "name": "Torque Limit(Application)","data": [],"unit": " %","valueDecimals": 0};
                      charts['56'] = {"graphKey": foreignKey + "-encloser_heater_on_off", "name": "Encloser Heater ON/OFF","data": [],"unit": "","valueDecimals": 0};
                      charts['57'] = {"graphKey": foreignKey + "-thawing_flag", "name": "Thawing Flag","data": [],"unit": "","valueDecimals": 0};
                      charts['58'] = {"graphKey": foreignKey + "-inlet_o2", "name": "Inlet O2","data": [],"unit": " %","valueDecimals": 0};
                      charts['59'] = {"graphKey": foreignKey + "-outlet_o2", "name": "Outlet O2","data": [],"unit": " %","valueDecimals": 0};

                      //--------------------------------------------------------------
                    }
                    

                    dtc_map.set(foreignKey, charts);
                  });


                  console.log('============dtc_map');
                  console.log(dtc_map.length);
                  console.log('============info_map');
                  console.log(info_map.length);



                  var JSONs = [];

                  var count = 1;

                  info_map.forEach(function(info, didx){
                    
                    var key = info[0];
                    var engine_serial_no = info[1]; //engineserialnof__c
                    var dtc_type = info[2];      //dtc_type__c  
                    var before7min = info[7];     //dtc_7minsago__c
                    var after3min = info[8];      //dtc_after3mins__c

                    console.log('[' + count + ']=============================================');
                    console.log('foreignKey = ' + key);
                    console.log('dtc_type = ' + dtc_type);
                    console.log('before7min = ' + before7min);
                    console.log('after3min = ' + after3min);
                    console.log('engine_serial_no = ' + engine_serial_no);
                    console.log('================================================');

                    //ECU ======================================================
                    if('ECU' == dtc_type){
                      rawData.forEach(function (line, idx) {

                        var unixtime = parseFloat(line[0]);

                        if(unixtime >= before7min && unixtime <= after3min && engine_serial_no == line[60]){
                          var cnt = 1;
                          _ECU.forEach(function(fieldIndex, eidx){
                            if(line[fieldIndex] === ''){
                              dtc_map.get(key)['' + fieldIndex].data.push(null);
                            } else {
                              dtc_map.get(key)['' + fieldIndex].data.push(parseFloat(line[fieldIndex]));
                            }
                            cnt++;
                          });
                          console.log('=========ecu data count = ' + cnt);
                        }
                      });


                    //ACU ======================================================
                    }else if('ACU' == dtc_type){
                      rawData.forEach(function (line, idx) {
                        var unixtime = parseFloat(line[0]);

                        if(unixtime >= before7min && unixtime <= after3min && engine_serial_no == line[60]){
                          var cnt = 1;
                          _ACU.forEach(function(fieldIndex, eidx){
                            //console.log(dtc_map.get(key));
                            if(line[fieldIndex] === ''){
                              dtc_map.get(key)['' + fieldIndex].data.push(null);
                            } else {
                              dtc_map.get(key)['' + fieldIndex].data.push(parseFloat(line[fieldIndex]));
                            }
                            cnt++;
                          });
                          console.log('=========acu data count = ' + cnt);
                        }
                      });

                      // valueDecimalsセット
                      for (var i = 0; i < indexOfValueDecimal.length; i++) {
                        // CCP項目のvalueDecimal代入
                        if (indexOfValueDecimal.includes(indexOfValueDecimal[i])) {
                          charts[indexOfValueDecimal[i]].valueDecimals = valueDecimal(charts[indexOfValueDecimal[i]].data);
                          console.log(
                            'i:' + i + '\n' +
                            'indexOfValueDecimal[i]: ' + indexOfValueDecimal[i] + '\n' +
                            'valueDecimals: ' + charts[indexOfValueDecimal[i]].valueDecimals + '\n'
                          );
                        }
                      }
                      
                    }
                    
                    var cnt = 1;
                    dtc_map.get(key).forEach(function(aRec,midx){
                      console.log('to mongo DB record data count = ' + aRec.data.length);
                      console.log(aRec);
                      JSONs.push(aRec);
                      cnt++;
                    });
                    console.log('' + cnt + ' records will be upserted into mongoDB');

                    //console.log('===========json');
                    //console.log(dtc_map.get(key));
                    count++;
                  });

                  /*JSONs.forEach(function(j, jidx){
                    console.log(j);
                  });
                  */
                  
                  callback(null, JSONs);


                }); // end of csv parse (td_history)
              }); // end of job result (td_history)
            }else{
              console.log('============this job is still processing...  waiting 10 seconds');
              sl.sleep(10);
              i++;
              loop();  
            }
          }); //end of show job (td history)
        }
      }()); //end of loop (td_history)
    }); // end of presto query
    //------------------------------------------------------------
  },





  //upsert mongoDB=================================
  function(JSONs, callback){

    console.log('=============upsert to mongoDB');

    var asyncTasks = [];

    // mongodb に投入
    var Graph = mongoose.model('Graph');
    JSONs.forEach(function (rec, idx) {
      asyncTasks.push(function (callback) { 
        Graph.update(
          { graphKey: rec.graphKey },
          { $set: {name:rec.name, data: rec.data, unit:rec.unit, valueDecimals:rec.valueDecimals}},
          { upsert: true, multi: true }, function (err) {
            
            if (err) { console.error(err);process.exit(1); }
            
            console.log('=============upsert callback()');
            callback();
        });
      });
    });

    async.parallel(asyncTasks, function () {
      console.log('===============job finished');
      process.exit();
    });
  }







/*,
  //get kwpccp sql=================================
  function(token, callback){
    fs.readFile('/home/ubuntu/TDSQL_DTCHISTORY3.sql', 'utf8', function(err, data){
      if(err){console.log(err);process.exit(1);}
      SQL_KWPCCP = data;
      console.log('================SQL_KWPCCP = ' + SQL_KWPCCP);
      callback(null);
    });
  }
*/
  ],





  function(err, result){
    if(err){console.log(err);process.exit(1);}
    console.log('=======end of waterfall function calls');
    process.exit(0);
  });
  //-------------------------------------------------

